This code uses MSG91 API to send SMS. Can be extended to send SMS via other services as well. It takes care of not bombarding same number with the SMS again and again upto a given time limit which can be edited in the smsservice.properties file.

To Build, go to project directory (containing pom.xml) and type

`$ mvn clean compile install`

To run, go to project directory (containing pom.xml) and type

`java -jar target/SmsService-1.0-SNAPSHOT.jar`