/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice;

import com.mycompany.smsservice.util.LogFactory;
import com.mycompany.smsservice.verticle.SmsRequestHttpVerticle;
import com.mycompany.smsservice.verticle.SmsRequestListenerVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;

/**
 *
 * @author sankalp.kulshrestha
 */
public class Main {

    private final static Logger LOGGER = LogFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOGGER.info("Starting Main");
        Vertx vertx = Vertx.vertx();

        SmsRequestListenerVerticle smsRequestListenerVerticle = new SmsRequestListenerVerticle();
        vertx.deployVerticle(smsRequestListenerVerticle);
        
        SmsRequestHttpVerticle smsRequestHttpVerticle = new SmsRequestHttpVerticle();
        vertx.deployVerticle(smsRequestHttpVerticle);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    LOGGER.info("Stopping SMSService");
                    smsRequestListenerVerticle.stop();
                    smsRequestHttpVerticle.stop();
                } catch (Exception ex) {
                    LOGGER.error(ex);
                }
            }
        });
    }

}
