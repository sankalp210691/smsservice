/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.exception;

/**
 *
 * @author sankalp.kulshrestha
 */
public class SmsSenderException extends RuntimeException{

    public SmsSenderException(String string) {
        super(string);
    }
    
}
