/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.smssender;

import com.mycompany.smsservice.exception.SmsSenderException;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

/**
 *
 * @author sankalp.kulshrestha
 */
public interface SmsSender {
    public void sendMessage(String requestId, String phone, String message, Handler<AsyncResult<Boolean>> handler) throws SmsSenderException;
    
    public void sendOtp(String requestId, String phone, String message, String otp, Handler<AsyncResult<Boolean>> handler) throws SmsSenderException;
}
