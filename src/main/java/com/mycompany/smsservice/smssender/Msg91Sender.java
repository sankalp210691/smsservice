/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.smssender;

import com.mycompany.smsservice.exception.SmsSenderException;
import com.mycompany.smsservice.util.Constants;
import com.mycompany.smsservice.util.LogFactory;
import com.mycompany.smsservice.util.Util;
import com.mycompany.smsservice.verticle.SmsRequestListenerVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 *
 * @author sankalp.kulshrestha
 */
public class Msg91Sender implements SmsSender {

    private final WebClient client;
    private final static Logger LOGGER = LogFactory.getLogger(SmsRequestListenerVerticle.class);

    public Msg91Sender(Vertx vertx) {
        WebClientOptions options = new WebClientOptions().setKeepAlive(true);
        client = WebClient.create(vertx, options);
    }

    @Override
    public void sendMessage(String requestId, String phone, String message, Handler<AsyncResult<Boolean>> handler) {
        boolean correctArgs = checkArguments(requestId, phone, message);
        if (!correctArgs) {
            handler.handle(Future.failedFuture(new SmsSenderException("One or more arguments are null or empty")));
        } else {
            try {
                StringBuilder urlBuilder = new StringBuilder(Constants.SMS_API).append("?")
                        .append("authkey=").append(Constants.SMS_API_KEY)
                        .append("&mobiles=").append(phone)
                        .append("&message=").append(URLEncoder.encode(message, "UTF-8"))
                        .append("&sender=").append(Constants.MSG91_SENDER_ID)
                        .append("&route=").append(Constants.MSG91_ROUTE);

                String apiUrl = urlBuilder.toString();
                send(requestId, apiUrl, handler);
            } catch (UnsupportedEncodingException ex) {
                handler.handle(Future.failedFuture(new SmsSenderException(ex + "")));
            }
        }
    }

    @Override
    public void sendOtp(String requestId, String phone, String message, String otp, Handler<AsyncResult<Boolean>> handler) throws SmsSenderException {
        //https://control.msg91.com/api/sendotp.php?authkey=178373AKWFRCdRdw3f59d9b84b&mobile=+91-9620301240&message=Your%20otp%20is%202786&sender=ABCD12&otp=2786
        boolean correctArgs = checkArguments(requestId, phone, message);
        if (!correctArgs) {
            handler.handle(Future.failedFuture(new SmsSenderException("One or more arguments are null or empty")));
        } else {
            try {
                StringBuilder urlBuilder = new StringBuilder(Constants.SMS_API).append("?")
                        .append("authkey=").append(Constants.SMS_API_KEY)
                        .append("&mobiles=").append(phone)
                        .append("&message=").append(URLEncoder.encode(message, "UTF-8"))
                        .append("&sender=").append(Constants.MSG91_SENDER_ID)
                        .append("&otp=").append(otp);

                String apiUrl = urlBuilder.toString();
                send(requestId, apiUrl, handler);
            } catch (UnsupportedEncodingException ex) {
                handler.handle(Future.failedFuture(new SmsSenderException(ex + "")));
            }
        }
    }

    private boolean checkArguments(String requestId, String phone, String message) {
        return !(Util.isNullOrEmpty(requestId) || Util.isNullOrEmpty(phone) || Util.isNullOrEmpty(message));
    }

    private void send(String requestId, String apiUrl, Handler<AsyncResult<Boolean>> handler) {
        LOGGER.debug(requestId + "\tAPI CALL: " + apiUrl);

        client.requestAbs(HttpMethod.GET, apiUrl).send(hndlr -> {
            if (hndlr.succeeded()) {
                HttpResponse<Buffer> response = hndlr.result();
                if (response.statusCode() == Constants.SUCCESS_STATUS_CODE) {
                    LOGGER.debug(requestId + "\tSUCCESS: " + apiUrl);
                    handler.handle(Future.succeededFuture(Boolean.TRUE));
                } else {
                    LOGGER.error(requestId + "\tFAIL: " + apiUrl);
                    handler.handle(Future.failedFuture(
                            new SmsSenderException("SMS API returned "
                                    + response.statusCode() + " code")));
                }
            } else {
                handler.handle(Future.failedFuture(new SmsSenderException(apiUrl + " FAILED because " + hndlr.cause().getMessage())));
            }
        });
    }

}
