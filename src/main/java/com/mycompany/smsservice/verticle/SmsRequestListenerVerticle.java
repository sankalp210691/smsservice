/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.verticle;

import com.mycompany.smsservice.smssender.Msg91Sender;
import com.mycompany.smsservice.smssender.SmsSender;
import com.mycompany.smsservice.util.Constants;
import com.mycompany.smsservice.util.LogFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.kafka.client.consumer.KafkaConsumer;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;
import java.time.Instant;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerConfig;

/**
 *
 * @author sankalp.kulshrestha
 */
public class SmsRequestListenerVerticle extends AbstractVerticle {

    private KafkaConsumer<String, String> consumer;
    private SmsSender smsSender;
    private RedisClient redis;
    private final static Logger LOGGER = LogFactory.getLogger(SmsRequestListenerVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        LOGGER.info("SmsRequestListenerVerticle started!");

        super.start();

        RedisOptions redisConfig = new RedisOptions().setHost(Constants.REDIS_HOST).setPort(Constants.REDIS_PORT);
        redis = RedisClient.create(vertx, redisConfig);
        smsSender = new Msg91Sender(vertx);

        Properties kafkaConsumerProperties = new Properties();
        kafkaConsumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, Constants.BOOTSTRAP_SERVERS_CONFIG);
        kafkaConsumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, Constants.KEY_DESERIALIZER_CLASS_CONFIG);
        kafkaConsumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, Constants.VALUE_DESERIALIZER_CLASS_CONFIG);
        kafkaConsumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, Constants.GROUP_ID_CONFIG);
        kafkaConsumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, Constants.ENABLE_AUTO_COMMIT_CONFIG);
        kafkaConsumerProperties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, Constants.AUTO_COMMIT_INTERVAL_MS_CONFIG);

        consumer = KafkaConsumer.create(vertx, kafkaConsumerProperties);

        consumer.subscribe(Constants.CONSUMER_TOPIC, ar -> {
            if (ar.succeeded()) {
                LOGGER.info("Subscribed to " + Constants.CONSUMER_TOPIC);
            } else {
                LOGGER.error("Could not subscribe to " + Constants.CONSUMER_TOPIC + " : " + ar.cause().getMessage());
            }
        });

        consumer.handler(record -> {
            try {
                LOGGER.debug(record.value());
                JsonObject json = new JsonObject(record.value());
                String phone = json.getString(Constants.PHONE_KEY).replace("+", "").replace("-", "");
                String message = json.getString(Constants.MESSAGE_KEY);
                String requestId = json.getString(Constants.REQUEST_ID_KEY);
                String otp = json.getString(Constants.OTP_KEY);

                JsonObject log = new JsonObject();
                log.put("requestId", requestId);
                log.put("details", json);
                redis.get(phone, res -> {
                    if (res.succeeded()) {
                        String result = res.result();
                        if (result == null) {
                            sendSms(requestId, phone, message, otp, redis, log, 1);
                        } else {
                            String[] resultArray = result.split(",");
                            int smsCount = Integer.parseInt(resultArray[0]);
                            long lastSentTimestamp = Long.parseLong(resultArray[1]);
                            long currentTimestamp = Instant.now().getEpochSecond();
                            boolean differenceCheck = currentTimestamp - lastSentTimestamp > Constants.SMS_RETRY_DIFFERENCE;
                            boolean smsCountCheck = smsCount < Constants.MAX_SMS_PER_DAY;
                            if (differenceCheck && smsCountCheck) {
                                sendSms(requestId, phone, message, otp, redis, log, smsCount + 1);
                            } else {
                                log.put("status", Constants.SMS_NOT_SENT);

                                String cause = differenceCheck ? Constants.DIFFERENCE_CHECK_CAUSE + lastSentTimestamp + "," + currentTimestamp : null;
                                if (cause == null) {
                                    cause = Constants.SMS_COUNT_CHECK_CAUSE + smsCount + "," + Constants.MAX_SMS_PER_DAY;
                                } else {
                                    cause += "|" + Constants.SMS_COUNT_CHECK_CAUSE + smsCount + "," + Constants.MAX_SMS_PER_DAY;
                                }
                                log.put("cause", cause);
                                LOGGER.info(log);
                            }
                        }
                    } else {
                        log.put("status", Constants.REDIS_CONNECTION_ERROR);
                        log.put("error", res.cause().getMessage());
                        LOGGER.error(log);
                    }
                });
            } catch (Exception ex) {
                LOGGER.error(ex);
            }

        });
    }

    @Override
    public void stop() throws Exception {
        if (consumer != null) {
            consumer.close(res -> {
                if (res.succeeded()) {
                    LOGGER.info("Consumer is now closed");
                } else {
                    LOGGER.error("Consumer close failed " + res.cause().getMessage());
                }
            });
        }
    }

    private void sendSms(String requestId, String phone, String message, String otp, RedisClient redis, JsonObject log, int count) {
        if (otp != null) {
            smsSender.sendOtp(requestId, phone, message, otp, handler -> {
                if (handler.succeeded()) {
                    log.put("status", Constants.SMS_SENT);
                    redis.setex(phone, Constants.REDIS_TTL, count + "," + Long.toString(Instant.now().getEpochSecond()), hndlr -> {
                        if (hndlr.succeeded()) {
                            log.put("status", Constants.SUCCESS);
                            LOGGER.info(log);
                        } else {
                            log.put("error", hndlr.cause().getMessage());
                            LOGGER.error(log);
                        }
                    });
                } else {
                    log.put("status", Constants.SMS_SENDING_FAILED);
                    log.put("error", handler.cause().getMessage());
                    LOGGER.error(log);
                }
            });
        } else {
            smsSender.sendMessage(requestId, phone, message, handler -> {
                if (handler.succeeded()) {
                    log.put("status", Constants.SMS_SENT);
                    redis.setex(phone, Constants.REDIS_TTL, count + "," + Long.toString(Instant.now().getEpochSecond()), hndlr -> {
                        if (hndlr.succeeded()) {
                            log.put("status", Constants.SUCCESS);
                            LOGGER.info(log);
                        } else {
                            log.put("error", hndlr.cause().getMessage());
                            LOGGER.error(log);
                        }
                    });
                } else {
                    log.put("status", Constants.SMS_SENDING_FAILED);
                    log.put("error", handler.cause().getMessage());
                    LOGGER.error(log);
                }
            });
        }
    }
}
