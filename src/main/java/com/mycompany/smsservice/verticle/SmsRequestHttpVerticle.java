/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.verticle;

import com.mycompany.smsservice.smssender.SmsSender;
import com.mycompany.smsservice.util.Constants;
import com.mycompany.smsservice.util.LogFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.redis.RedisClient;
import java.time.Instant;

/**
 *
 * @author sankalpkulshrestha
 */
public class SmsRequestHttpVerticle extends AbstractVerticle {

    private SmsSender smsSender;
    private RedisClient redis;
    private final static Logger LOGGER = LogFactory.getLogger(SmsRequestListenerVerticle.class);

    private HttpServer httpServer = null;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        httpServer = vertx.createHttpServer();

        //TODO: This is GET right now. Should be POST
        httpServer.requestHandler(new Handler<HttpServerRequest>() {
            @Override
            public void handle(HttpServerRequest request) {
                LOGGER.debug(request.absoluteURI());
                String phone = request.getParam(Constants.PHONE_KEY);
                String message = request.getParam(Constants.MESSAGE_KEY);
                String requestId = request.getParam(Constants.REQUEST_ID_KEY);
                String otp = request.getParam(Constants.OTP_KEY);

                JsonObject log = new JsonObject();
                log.put("requestId", requestId);
                log.put("details", request.absoluteURI());

                HttpServerResponse response = request.response();

                redis.get(phone, res -> {
                    if (res.succeeded()) {
                        String result = res.result();
                        if (result == null) {
                            sendSms(requestId, phone, message, otp, redis, log, 1, handler -> {
                                if (handler.succeeded()) {
                                    response.write(Constants.SENT);
                                } else {
                                    response.write(Constants.NOT_SENT);
                                }
                                response.end();
                            });
                        } else {
                            String[] resultArray = result.split(",");
                            int smsCount = Integer.parseInt(resultArray[0]);
                            long lastSentTimestamp = Long.parseLong(resultArray[1]);
                            long currentTimestamp = Instant.now().getEpochSecond();
                            boolean differenceCheck = currentTimestamp - lastSentTimestamp > Constants.SMS_RETRY_DIFFERENCE;
                            boolean smsCountCheck = smsCount < Constants.MAX_SMS_PER_DAY;
                            if (differenceCheck && smsCountCheck) {
                                sendSms(requestId, phone, message, otp, redis, log, smsCount + 1, handler -> {
                                    if (handler.succeeded()) {
                                        response.write(Constants.SENT);
                                    } else {
                                        response.write(Constants.NOT_SENT);
                                    }
                                    response.end();
                                });
                            } else {
                                log.put("status", Constants.SMS_NOT_SENT);

                                String cause = differenceCheck ? Constants.DIFFERENCE_CHECK_CAUSE + lastSentTimestamp + "," + currentTimestamp : null;
                                if (cause == null) {
                                    cause = Constants.SMS_COUNT_CHECK_CAUSE + smsCount + "," + Constants.MAX_SMS_PER_DAY;
                                } else {
                                    cause += "|" + Constants.SMS_COUNT_CHECK_CAUSE + smsCount + "," + Constants.MAX_SMS_PER_DAY;
                                }
                                log.put("cause", cause);
                                LOGGER.info(log);
                                response.write(Constants.NOT_SENT);
                                response.end();
                            }
                        }
                    } else {
                        log.put("status", Constants.REDIS_CONNECTION_ERROR);
                        log.put("error", res.cause().getMessage());
                        LOGGER.error(log);
                        response.write(Constants.NOT_SENT);
                        response.end();
                    }
                });
            }
        });
        httpServer.listen(Constants.SERVER_PORT);
    }

    @Override
    public void stop() throws Exception {

    }

    private void sendSms(String requestId, String phone, String message, String otp, RedisClient redis, JsonObject log, int count, Handler<AsyncResult<Boolean>> hnd) {
        if (otp != null) {
            smsSender.sendOtp(requestId, phone, message, otp, handler -> {
                if (handler.succeeded()) {
                    log.put("status", Constants.SMS_SENT);
                    redis.setex(phone, Constants.REDIS_TTL, count + "," + Long.toString(Instant.now().getEpochSecond()), hndlr -> {
                        if (hndlr.succeeded()) {
                            log.put("status", Constants.SUCCESS);
                            LOGGER.info(log);
                            hnd.handle(Future.succeededFuture(Boolean.TRUE));
                        } else {
                            log.put("error", hndlr.cause().getMessage());
                            LOGGER.error(log);
                            hnd.handle(Future.succeededFuture(Boolean.FALSE));
                        }
                    });
                } else {
                    log.put("status", Constants.SMS_SENDING_FAILED);
                    log.put("error", handler.cause().getMessage());
                    LOGGER.error(log);
                    hnd.handle(Future.succeededFuture(Boolean.FALSE));
                }
            });
        } else {
            smsSender.sendMessage(requestId, phone, message, handler -> {
                if (handler.succeeded()) {
                    log.put("status", Constants.SMS_SENT);
                    redis.setex(phone, Constants.REDIS_TTL, count + "," + Long.toString(Instant.now().getEpochSecond()), hndlr -> {
                        if (hndlr.succeeded()) {
                            log.put("status", Constants.SUCCESS);
                            LOGGER.info(log);
                            hnd.handle(Future.succeededFuture(Boolean.TRUE));
                        } else {
                            log.put("error", hndlr.cause().getMessage());
                            LOGGER.error(log);
                            hnd.handle(Future.succeededFuture(Boolean.FALSE));
                        }
                    });
                } else {
                    log.put("status", Constants.SMS_SENDING_FAILED);
                    log.put("error", handler.cause().getMessage());
                    LOGGER.error(log);
                    hnd.handle(Future.succeededFuture(Boolean.FALSE));
                }
            });
        }
    }
}
