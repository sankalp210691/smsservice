/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.util;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

/**
 *
 * @author sankalp.kulshrestha
 */
public final class LogFactory {

    public static Logger getLogger(Class clazz) {
        Logger logger = null;
        try {
            final InputStream inputStream = new FileInputStream(new File("/disk1/smsservice.properties"));
            LogManager.getLogManager().readConfiguration(inputStream);
            logger = LoggerFactory.getLogger(clazz.getName());
            logger.info("LogFactory initialized");
        } catch (IOException | SecurityException ex) {
            ex.printStackTrace();
        }
        return logger;
    }
}
