/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.smsservice.util;

import io.vertx.core.logging.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerConfig;

/**
 *
 * @author sankalp.kulshrestha
 */
public class Constants {

    private static final Properties PROPERTIES = new Properties();
    private final static Logger LOGGER = LogFactory.getLogger(Constants.class);
    public static final String PHONE_KEY = "phone";
    public static final String MESSAGE_KEY = "message";
    public static final String REQUEST_ID_KEY = "requestId";
    public static final String OTP_KEY = "otp";
    public static final Integer SUCCESS_STATUS_CODE = 200;

    static {
        try {
            PROPERTIES.load(new FileInputStream(new File("/disk1/smsservice.properties")));
            if (!PROPERTIES.containsKey("SMS_API")) {
                LOGGER.error("SMS_API property not found in /disk1/smsservice.properties");
                System.exit(0);
            }
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex);
            System.exit(0);
        } catch (IOException ex) {
            LOGGER.error(ex);
            System.exit(0);
        }
    }
    public static final String REDIS_HOST = PROPERTIES.getProperty("REDIS_HOST", "127.0.0.1");
    public static final Integer REDIS_PORT = Integer.parseInt(PROPERTIES.getProperty("REDIS_PORT", "6379"));
    public static final Integer REDIS_TTL = Integer.parseInt(PROPERTIES.getProperty("REDIS_TTL", "86400"));
    public static final Integer SMS_RETRY_DIFFERENCE = Integer.parseInt(PROPERTIES.getProperty("SMS_RETRY_DIFFERENCE", "30"));
    public static final Integer MAX_SMS_PER_DAY = Integer.parseInt(PROPERTIES.getProperty("MAX_SMS_PER_DAY", "5"));
    
    
    public static final Integer SERVER_PORT = Integer.parseInt(PROPERTIES.getProperty("SERVER_PORT", "9990"));
    public static final String SENT = "SENT";
    public static final String NOT_SENT = "NOT SENT";
    
    public static final String SMS_SENT = "SMS_SENT";
    public static final String SUCCESS = "SUCCESS";
    public static final String SMS_SENDING_FAILED = "SMS_SENDING_FAILED";
    public static final String REDIS_CONNECTION_ERROR = "REDIS_CONNECTION_ERROR";
    public static final String SMS_NOT_SENT = "SMS_NOT_SENT";
    public static final String DIFFERENCE_CHECK_CAUSE = "SENDING SMS TOO SOON: ";
    public static final String SMS_COUNT_CHECK_CAUSE = "SMS COUNT PER DAY EXCEEDED: ";
    public static final String SMS_API = PROPERTIES.getProperty("SMS_API");
    public static final String SMS_API_KEY = PROPERTIES.getProperty("SMS_API_KEY");
    public static final String MSG91_ROUTE = PROPERTIES.getProperty("MSG91_ROUTE", "default");
    public static final String MSG91_SENDER_ID = PROPERTIES.getProperty("MSG91_SENDER_ID", "123456");

    public static final String BOOTSTRAP_SERVERS_CONFIG = PROPERTIES
            .getProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
    public static final String KEY_DESERIALIZER_CLASS_CONFIG = "org.apache.kafka.common.serialization.StringDeserializer";
    public static final String VALUE_DESERIALIZER_CLASS_CONFIG = "org.apache.kafka.common.serialization.StringDeserializer";
    public static final String GROUP_ID_CONFIG = PROPERTIES
            .getProperty(ConsumerConfig.GROUP_ID_CONFIG, "SMS_CONSUMER");
    public static final String CONSUMER_TOPIC = PROPERTIES.getProperty("consumerTopic", "sms");
    public static final Boolean ENABLE_AUTO_COMMIT_CONFIG = "true".equals(PROPERTIES.getProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG));
    public static final Integer AUTO_COMMIT_INTERVAL_MS_CONFIG = Integer.parseInt(PROPERTIES.getProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG));
}
